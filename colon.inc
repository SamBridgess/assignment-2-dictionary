%define dictionary_begin 0

%macro colon 2
    %ifid %2
        %2:
        dq dictionary_begin
    %else
        %error "Not an ID"
    %endif
    %ifstr %1
        db %1, 0
    %else
        %error "Not a string"
    %endif
    %define dictionary_begin %2
%endmacro