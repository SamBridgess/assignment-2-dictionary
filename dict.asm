
section .text
extern string_equals

;rdi - pointer to the null-terminated string 
;rsi - pointer to the begging of dictionary

global find_word
find_word:

		
		add rsi, 8
		push rdi
		push rsi
		call string_equals
		pop rsi
		pop rdi
		
		sub rsi, 8
		test rax, rax
		jnz .ret
		
		mov rsi, [rsi]
		test rsi, rsi
		jnz find_word
.ret:
    mov rax, rsi
    ret
	
;rdi
;rsi
;rdx
