section .text
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    .loop:
		xor rax, rax
    .count:
		cmp byte [rdi+rax], 0
		je .end
		inc rax
		jmp .count
    .end:
		ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
	pop rdi
    mov  rdx, rax	; string length in rdx
    mov  rsi, rdi		;data      from rdi to rsi (in rsi we have a pointer to where the output data is located)
    mov  rax, 1		;command num(1 for sys_write)
    mov  rdi, 1		; stdout
    syscall
    ret
 	

; Принимает код символа и выводит его в stdout
print_char:
	dec rsp
	mov [rsp], dil
	mov rsi, rsp
	mov rdi, 1
	mov rax, 1
	mov rdx, 1
	syscall
	inc rsp
	ret
; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 10
    jmp print_char
	


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint: 

    mov rax, rdi
	mov r9, 1			;counter
	mov r10, 10
	
    dec rsp
    mov byte [rsp], 0 

	.loop:
		mov rdx, 0 
		div r10
		
		add rdx, '0'  		; make char
		
		dec rsp    		
		mov [rsp], dl 	
		
		inc r9
		
		cmp rax, 0 
		jnz .loop
		
	mov rdi, rsp 
	call print_string
	add rsp, r9
	
	ret
   

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int: 
	cmp rdi, 0
	jl .add_minus_sign
	.print_and_ret:
		jmp print_uint
	.add_minus_sign:
		push rdi
		mov rdi, '-'	;minus
		call print_char
		pop rdi
		neg rdi
		jmp .print_and_ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе+++
string_equals:
    xor rax, rax
	push rbx		;save rbx to stack in case user had something in it
	mov rbx, 0	;cycle counter
	.loop:
		mov r9b, [rdi+rbx]
		mov r10b, [rsi+rbx]
		inc rbx		;cycle counter++
		cmp r9b, r10b
		jne .ret		;return 0 if not equels
		cmp r9b, 0
		
		jnz .loop
		mov rax, 1
	.ret:
		pop rbx		;retreave rbx from stack
		ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока+++
read_char:
	dec rsp
    xor rdi, rdi		;stdin operation
	xor rax, rax 	;sys_write
	mov rdx, 1  	;number of symbols
	mov rsi, rsp	;we put stack pointer in rsi so that input data will be stored in stack
	syscall
	cmp rax, 0
	jz .ret
	mov al, [rsp]
	.ret:
		inc rsp
		ret
    

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
	cmp rsi, 0
    jne .cycle_begin

    
	.buffer_error_ret_0:
        xor rax, rax
        ret
	
	.cycle_begin:
	xor rax, rax
    xor rdx, rdx
    .loop:
	    push rdx		;saving into stack before callint read_char, which uses those registers
        push rdi
        push rsi

        call read_char
		pop rsi
		pop rdi
        pop rdx

		
		mov r9b, al
		cmp r9b, 9	 	;tab
        je .space_symbol
        cmp r9b, 10		;line feed
        je .space_symbol
        cmp r9b, 32  	;space
        je .space_symbol

        mov byte[rdi + rdx], r9b
        cmp r9b, 0
        je .ret
        
		inc rdx
		
		cmp rsi, rdx
		je .buffer_error_ret_0
		jmp .loop
		
		.space_symbol:
		cmp rdx, 0	
		je .loop
		
    .ret:
        mov rax, rdi
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	mov rax, 0
	mov rsi, 0		;counter
	mov r10, 10	;stores 10 to achieve shift after multipling rax*10
	
	.loop:
		mov r9, 0
		mov r9b, [rdi+rsi]
		cmp r9, '9'	;check if > 9(char)
		jg .ret
		cmp r9, '0'	;check if < 0(char)
		jl .ret

		
		mul r10				;multipling to 10 to make a shifts
		add rax, r9		;adding a new digit
		sub rax, '0'	;sub '0' because its char
		
		inc rsi
		jmp .loop
	.ret:
		mov rdx, rsi
		ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int: 
    mov r8b, 0
	cmp byte[rdi], 45		;new line
	je .negative
	.positive:
		push r8
		call parse_uint
		pop r8
		cmp rdx, 0 
		jz .ret
		cmp r8b, 0
		jz .ret
		neg rax
		inc rdx
		.ret:
			ret	
	.negative:
		not r8b			;set negative flag to true if number is negative
		inc rdi
		jmp .positive		

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	call string_length

	cmp rax, rdx
	jg .ret_null

	xor rax, rax
	.loop:
		inc rax;		counter
		cmp rax, rdx
		jg .ret_null		;return 0 if there is not enough space in buffer
		mov r9b, [rdi+rax-1] ;copy one byte from string to compare to 0
		cmp r9b, 0
		mov [rsi+rax-1], r9b ;copy this byte to buffer 
		jnz .loop
		
	dec rax	;dec counter because of an additional inc
	ret
	
	.ret_null:
		xor rax, rax
		ret

;rdi
;rsi
;rdx
