%include 'words.inc'
%include 'lib.inc'

section .const
col: db ": ", 0
enter_key_message: db "Enter key: ", 0
key_not_found_message: db "There is no such key", 0xA, 0
key_too_long_message: db "The key you've entered is longer than 255 simbols!", 0xA, 0

section .bss
buffer: resb 256

section .text

global print_error
print_error:
    push rdi            
    call string_length
    pop rdi
    mov rdx, rax
    mov rax, 1
    mov rsi, rdi
    mov rdi, 2
    syscall
    ret   

global _start
_start:
    mov rdi, enter_key_message
    call print_string

	xor r9, r9				;symbol counter
    sub rsp, 256    			
.get_string:
    ;push r9
    call read_char
    ;pop r9

    cmp rax, 0xA          ;end of string
    je .input_end
    cmp rax, 0x0
    je .input_end

    cmp r9, 255     		
    jl .contunue1
	mov rdi, key_too_long_message   ;key is too long
    call print_error
	jmp exit
	
	.contunue1:
    mov [buffer+r9], rax
    inc r9
    jmp .get_string
	
	
.input_end:
    mov byte [buffer+r9], 0		;place terminator
	
	

mov rdi, buffer
mov rsi, dictionary_begin

call find_word

test rax, rax
jnz .contunue2

mov rdi, key_not_found_message 		;word not found
call print_error
jmp .exit		


.contunue2:
	mov rdi, rax
	add rdi, 8				;next adr

	push rdi
	call print_string		;print key
	mov rdi, col			
	call print_string		;print ":"
	pop rdi

	call string_length
	lea rdi, [rdi+rax+1]

	call print_string
	call print_newline

.exit:
    call exit