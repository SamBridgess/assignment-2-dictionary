
run: clean main

%.o: %.asm
	nasm -f elf64 $< -o $@

main: main.o dict.o lib.o
	ld -o $@ $^


clean:
	rm -rf *.o main
	
.PHONY: clean run
